FROM python:3.8-buster

RUN apt-get update && apt-get install -y \
        git \
        awscli \
        && \
    pip install awsebcli --upgrade